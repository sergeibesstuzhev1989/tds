// Fill out your copyright notice in the Description page of Project Settings.


#include "MyProjectile_Grenade.h"
#include "Kismet/GameplayStatics.h"

void AMyProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AMyProjectile_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AMyProjectile_Grenade::TimerExplose(float DeltaTime)
{
	if(TimerEnabled)
	{
		if(TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else TimerToExplose += DeltaTime;
	}
}

void AMyProjectile_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp,
														AActor* OtherActor,
														UPrimitiveComponent* OtherComp,
														FVector NormalImpulse,
														const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AMyProjectile_Grenade::ImpactProjectile()
{
	//Super::ImpactProjectile();
	// init Grenade
	TimerEnabled = true;
}

void AMyProjectile_Grenade::Explose()
{
	TimerEnabled = false;
	if(ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
													ProjectileSetting.ExploseFX,
													GetActorLocation(),
													GetActorRotation(),
													FVector(1.f));
	}
	if(ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),
												ProjectileSetting.ExploseSound,
												GetActorLocation());
	}

	const TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
	                                               ProjectileSetting.ExploseMaxDamage,
	                                               ProjectileSetting.ExploseMaxDamage * 0.2f,
	                                               GetActorLocation(),
	                                               1000.f,
	                                               2000.f,
	                                               5,
	                                               nullptr,
	                                               IgnoredActor,
	                                               nullptr,
	                                               nullptr);

	this->Destroy();
}
