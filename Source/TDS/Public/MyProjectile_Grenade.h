// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "MyProjectile_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AMyProjectile_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	// called the game start
	virtual void BeginPlay() override;

public:
	// called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp,
											AActor* OtherActor,
											UPrimitiveComponent* OtherComp,
											FVector NormalImpulse,
											const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	void Explose();

	bool TimerEnabled = false;
	float TimerToExplose = 0.f;
	float TimeToExplose = 5.f;
};
