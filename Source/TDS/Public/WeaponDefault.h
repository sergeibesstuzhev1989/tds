// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TDS/Public/MyTypes.h"
#include "TDS/Public/ProjectileDefault.h"
#include "Kismet/GameplayStatics.h"
#include "WeaponDefault.generated.h"

// DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart); ToDo Event weapon fire
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true), Category = Component)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true), Category = Component)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true), Category = Component)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true), Category = Component)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
	FWeaponInfo WeaponSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAddicionalWeaponInfo WeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);

	UFUNCTION()
	void WeaponInit();

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();
	UFUNCTION()
	void InitReload();
	UFUNCTION()
	void FinishReload();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
	bool WeaponIsFiring = false;

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool IsFiring);
	
	bool CheckWeaponCanFire();
	
	FProjectileInfo GetProjectile();

	UFUNCTION()
	void Fire();
	UFUNCTION()
	void ChangeDispersionByShot();
	UFUNCTION()
	float GetCurrentDispersion() const;
	UFUNCTION()
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	UFUNCTION()
	int8 GetNumberProjectileByShot() const;
	UFUNCTION()
	FVector GetFireEndLocation() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
	bool WeaponReloading = false;

	// Flags
	bool BlockFire = false;

	// Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.f;
	float CurrentDispersionMax = 1.f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	FVector ShootEndLocation = FVector(0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	
	//Timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")	//Remove !!! Debug
	float ReloadTime = 0.0f;

	UFUNCTION()
	void UpdateStateWeapon(EMovementState NewMovementState);

	UPROPERTY()
	float FireTime = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.f;
 
};
